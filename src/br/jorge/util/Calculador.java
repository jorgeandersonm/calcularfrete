package br.jorge.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceActivity.Header;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import br.jorge.model.FreteCalculado;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class Calculador {

	private Context context;

	private EditText cepOrigem;
	private EditText cepDestino;
	private EditText peso;
	private EditText valorDeclarado;

	private FreteCalculado frete;
	private ProgressBar progressBar;
	private Button button;
	CheckBox declarar;

	public void calcular(EditText cepOrigem, EditText cepDestino,
			EditText peso, EditText valorDeclarado, CheckBox declararCheckBox,
			ProgressBar pb, Button calcular, Context context) {

		this.context = context;
		this.cepDestino = cepDestino;
		this.cepOrigem = cepOrigem;
		this.peso = peso;
		this.valorDeclarado = valorDeclarado;
		this.progressBar = pb;
		this.button = calcular;
		this.declarar = declararCheckBox;

		disable();

//		ConnectivityManager cm = (ConnectivityManager) context
//				.getSystemService(Context.CONNECTIVITY_SERVICE);
//		NetworkInfo netInfo = cm.getActiveNetworkInfo();
//
//		if (netInfo.isConnectedOrConnecting() == false) {
//			enable();
//			Toast.makeText(context, "Sem conex�o", Toast.LENGTH_SHORT).show();
//		}

		if (cepOrigem.length() != 8 || cepDestino.length() != 8) {
			enable();
			Toast.makeText(context, "CEP inv�lido", Toast.LENGTH_SHORT).show();
		}

		else if (peso.getText().toString().isEmpty()) {
			enable();
			Toast.makeText(context, "Preencha o peso", Toast.LENGTH_SHORT)
					.show();
		}

		else if (Double.parseDouble(peso.getText().toString()) < 0.3
				|| Double.parseDouble(peso.getText().toString()) > 30.0) {
			enable();
			Toast.makeText(context,
					"O peso deve ser maior que 0.300g e menor que 30.0kg",
					Toast.LENGTH_SHORT).show();
		}

		else if (declararCheckBox.isChecked()) {
			if (valorDeclarado.getText().toString().isEmpty()) {
				enable();
				Toast.makeText(context, "Preencha o valor declarado",
						Toast.LENGTH_SHORT).show();
			} else if (Double.parseDouble(valorDeclarado.getText().toString()) > 10000.0) {
				enable();
				Toast.makeText(context,
						"O valor m�ximo a declarar � de R$ 10000.00",
						Toast.LENGTH_SHORT).show();
			} else {
				calcularComValorDeclarado();
			}
		} else {
			calcularSemValorDeclarado();
		}
	}

	public void calcularComValorDeclarado() {
		String urlString = "http://developers.agenciaideias.com.br/correios/frete/json/"
				+ cepOrigem.getText().toString()
				+ "/"
				+ cepDestino.getText().toString()
				+ "/"
				+ peso.getText().toString()
				+ "/"
				+ valorDeclarado.getText().toString();

		AsyncHttpClient client = new AsyncHttpClient();
		client.get(urlString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
				Gson gson = new Gson();
				frete = gson.fromJson(response, FreteCalculado.class);
				
				if(frete.getPac() == null || frete.getSedex() == null)
					erro(context);
				else
					ok(context);

			}

			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				erro(context);

			}
		});
	}

	public void calcularSemValorDeclarado() {
		String urlString = "http://developers.agenciaideias.com.br/correios/frete/json/"
				+ cepOrigem.getText().toString()
				+ "/" 
				+ cepDestino.getText().toString()
				+ "/" 
				+ peso.getText().toString();

		AsyncHttpClient client = new AsyncHttpClient();
		client.get(urlString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
				Gson gson = new Gson();
				frete = gson.fromJson(response, FreteCalculado.class);
				if(frete.getPac() == null || frete.getSedex() == null)
					erro(context);
				else
					ok(context);
			}

			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				erro(context);

			}
		});
	}

	public void erro(Context context) {
		enable();
		Toast.makeText(context, "Algo deu errado :(", Toast.LENGTH_SHORT).show();
	}

	public void ok(Context context) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage("Sedex: R$" + frete.getSedex() + "\n" + "PAC: R$"
				+ frete.getPac());
		builder.setTitle("Resultado");
		builder.setPositiveButton("Voltar",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

		AlertDialog alert = builder.create();
		enable();
		alert.show();
	}

	public void disable() {
		cepOrigem.setEnabled(false);
		cepDestino.setEnabled(false);
		peso.setEnabled(false);
		valorDeclarado.setEnabled(false);
		button.setEnabled(false);
		declarar.setEnabled(false);

		progressBar.setVisibility(View.VISIBLE);
	}

	public void enable() {
		cepOrigem.setEnabled(true);
		cepDestino.setEnabled(true);
		peso.setEnabled(true);
		valorDeclarado.setEnabled(true);
		button.setEnabled(true);
		declarar.setEnabled(true);
		
		if(declarar.isChecked())
			valorDeclarado.setEnabled(true);
		else
			valorDeclarado.setEnabled(false);

		progressBar.setVisibility(View.INVISIBLE);
	}
}
