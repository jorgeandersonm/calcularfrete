package br.jorge.calculofrete;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import br.jorge.util.Calculador;

public class MainActivity extends ActionBarActivity {
	
	EditText cepOrigem;
	EditText cepDestino;
	EditText peso;
	EditText valorDeclarado;
	CheckBox declararCheckBox;
	Button calcular;
	ProgressBar progressBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		instanciarValores();
		
		valorDeclarado.setEnabled(false);
		progressBar.setVisibility(View.INVISIBLE);

	}
	
	public void instanciarValores(){
		cepOrigem = (EditText) findViewById(R.id.cepOrigem);
		cepDestino = (EditText) findViewById(R.id.cepDestino);
		peso = (EditText) findViewById(R.id.peso);
		valorDeclarado = (EditText) findViewById(R.id.valorDeclarado);
		declararCheckBox = (CheckBox) findViewById(R.id.declararCheckBox);
		calcular = (Button) findViewById(R.id.calcular);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
	}
	
	
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar


		getMenuInflater().inflate(R.menu.main_activity_actions, menu);
	    MenuItem searchItem = menu.findItem(R.id.action_info);
	    SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
			
	    return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.action_info:
	            info();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	public void onCheckboxClicked(View view) {
		
	    if(declararCheckBox.isChecked())
	    	valorDeclarado.setEnabled(true);
	    else
	    	valorDeclarado.setEnabled(false);
	}
	
	public void info(){
		Intent intent = new Intent(this, AboutActivity.class);
		startActivity(intent);
	}
	
	public void calcular(View view){
		Calculador calculador = new Calculador();
		
		if (isNetworkAvailable(this) == false) {
			Toast.makeText(this, "Sem conex�o", Toast.LENGTH_SHORT).show();
		}
		else{
		calculador.calcular(cepOrigem, 
				cepDestino, peso, 
				valorDeclarado, declararCheckBox, 
				progressBar, calcular, MainActivity.this);
		}
	}
	
	public static void desabilitar(){
		
	}
	
	public static boolean isNetworkAvailable(Context context) {
	    return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
	}
}
