package br.jorge.model;

public class FreteCalculado {

	private String sedex;
	private String pac;
	
	public FreteCalculado(String sedex, String pac) {
		super();
		this.sedex = sedex;
		this.pac = pac;
	}

	public String getSedex() {
		return sedex;
	}

	public void setSedex(String sedex) {
		this.sedex = sedex;
	}

	public String getPac() {
		return pac;
	}

	public void setPac(String pac) {
		this.pac = pac;
	}	
}
